<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class KategoriHelpDesk
 * 
 * @property int $id
 * @property string $nama_kategori
 * @property int $waktu_tunggu
 * 
 * @property Collection|HelpDesk[] $help_desks
 *
 * @package App\Models
 */
class KategoriHelpDesk extends Model
{
	protected $table = 'kategori_help_desk';
	public $timestamps = false;

	protected $casts = [
		'waktu_tunggu' => 'int'
	];

	protected $fillable = [
		'nama_kategori',
		'waktu_tunggu'
	];

	public function help_desks()
	{
		return $this->hasMany(HelpDesk::class);
	}
}
