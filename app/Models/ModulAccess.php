<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ModulAccess
 * 
 * @property int $id
 * @property string $access
 * @property int $modul_id
 * 
 * @property Modul $modul
 * @property Collection|Role[] $roles
 *
 * @package App\Models
 */
class ModulAccess extends Model
{
	protected $table = 'modul_access';
	public $timestamps = false;

	protected $casts = [
		'modul_id' => 'int'
	];

	protected $fillable = [
		'access',
		'modul_id'
	];

	public function modul()
	{
		return $this->belongsTo(Modul::class);
	}

	public function roles()
	{
		return $this->belongsToMany(Role::class, 'roles_modul_access', 'modul_access_id', 'roles_id')
					->withPivot('id');
	}
}
