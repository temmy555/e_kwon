<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Role
 * 
 * @property int $id
 * @property string $nama_role
 * @property string $keterangan
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property Collection|Modul[] $moduls
 * @property Collection|ModulAccess[] $modul_accesses
 * @property Collection|User[] $users
 *
 * @package App\Models
 */
class Role extends Model
{
	use SoftDeletes;
	protected $table = 'roles';

	protected $fillable = [
		'nama_role',
		'keterangan'
	];

	public function moduls()
	{
		return $this->belongsToMany(Modul::class, 'roles_modul', 'roles_id')
					->withPivot('id', 'parent_id', 'is_hidden');
	}

	public function modul_accesses()
	{
		return $this->belongsToMany(ModulAccess::class, 'roles_modul_access', 'roles_id')
					->withPivot('id');
	}

	public function users()
	{
		return $this->hasMany(User::class, 'roles_id');
	}
}
