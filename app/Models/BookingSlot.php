<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BookingSlot
 * 
 * @property int $id
 * @property int $klub_id
 * @property int $acara_id
 * @property string $no_invoice
 * @property int $jml_slot
 * @property int $jml_pelatih
 * @property int $verified
 * @property float $biaya_total
 * @property string $url_foto_bukti_tf
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property Acara $acara
 * @property Klub $klub
 *
 * @package App\Models
 */
class BookingSlot extends Model
{
	use SoftDeletes;
	protected $table = 'booking_slot';

	protected $casts = [
		'klub_id' => 'int',
		'acara_id' => 'int',
		'jml_slot' => 'int',
		'jml_pelatih' => 'int',
		'verified' => 'int',
		'biaya_total' => 'float'
	];

	protected $fillable = [
		'klub_id',
		'acara_id',
		'no_invoice',
		'jml_slot',
		'jml_pelatih',
		'verified',
		'biaya_total',
		'url_foto_bukti_tf'
	];

	public function acara()
	{
		return $this->belongsTo(Acara::class);
	}

	public function klub()
	{
		return $this->belongsTo(Klub::class);
	}
}
