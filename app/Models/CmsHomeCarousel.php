<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CmsHomeCarousel
 * 
 * @property int $id
 * @property string $nama_home_carousel
 * @property string $img
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class CmsHomeCarousel extends Model
{
	use SoftDeletes;
	protected $table = 'cms_home_carousel';
	public $incrementing = false;

	protected $casts = [
		'id' => 'int'
	];

	protected $fillable = [
		'nama_home_carousel',
		'img'
	];
}
