<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class HelpDesk
 * 
 * @property int $id
 * @property string $subjek
 * @property string $isi
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 * @property int $kategori_help_desk_id
 * @property int $users_id
 * 
 * @property KategoriHelpDesk $kategori_help_desk
 * @property User $user
 * @property Collection|HelpDeskDetail[] $help_desk_details
 *
 * @package App\Models
 */
class HelpDesk extends Model
{
	use SoftDeletes;
	protected $table = 'help_desk';

	protected $casts = [
		'kategori_help_desk_id' => 'int',
		'users_id' => 'int'
	];

	protected $fillable = [
		'subjek',
		'isi',
		'kategori_help_desk_id',
		'users_id'
	];

	public function kategori_help_desk()
	{
		return $this->belongsTo(KategoriHelpDesk::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class, 'users_id');
	}

	public function help_desk_details()
	{
		return $this->hasMany(HelpDeskDetail::class);
	}
}
