<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Kela
 * 
 * @property int $id
 * @property string $kategori_kelas
 * @property string $kelas
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property Collection|Atlet[] $atlets
 * @property Collection|KategoriAcara[] $kategori_acaras
 *
 * @package App\Models
 */
class Kela extends Model
{
	use SoftDeletes;
	protected $table = 'kelas';

	protected $fillable = [
		'kategori_kelas',
		'kelas'
	];

	public function atlets()
	{
		return $this->hasMany(Atlet::class, 'kelas_id');
	}

	public function kategori_acaras()
	{
		return $this->hasMany(KategoriAcara::class, 'kelas_id');
	}
}
