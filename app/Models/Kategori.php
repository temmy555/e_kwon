<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Kategori
 * 
 * @property int $id
 * @property string $kategori
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property Collection|Acara[] $acaras
 *
 * @package App\Models
 */
class Kategori extends Model
{
	use SoftDeletes;
	protected $table = 'kategori';

	protected $fillable = [
		'kategori'
	];

	public function acaras()
	{
		return $this->belongsToMany(Acara::class, 'kategori_acara')
					->withPivot('id', 'atlet_id', 'kelas_id', 'juara_id', 'pengkot_id');
	}
}
