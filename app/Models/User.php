<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class User
 *
 * @property int $id
 * @property string $username
 * @property int $roles_id
 * @property string $password
 * @property string $nama
 * @property string $keterangan
 * @property int $verified
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property Role $role
 * @property Collection|Atlet[] $atlets
 * @property Collection|HelpDesk[] $help_desks
 * @property Collection|HelpDeskDetail[] $help_desk_details
 * @property Collection|Klub[] $klubs
 * @property Collection|Pengkot[] $pengkots
 *
 * @package App\Models
 */
class User extends Authenticatable
{
    use SoftDeletes;
	protected $table = 'users';

	protected $casts = [
		'roles_id' => 'int',
		'verified' => 'int'
	];

	protected $hidden = [
		'password'
	];

	protected $fillable = [
		'username',
		'roles_id',
		'password',
		'nama',
		'keterangan',
		'verified'
	];

	public function role()
	{
		return $this->belongsTo(Role::class, 'roles_id');
	}

	public function atlets()
	{
		return $this->hasMany(Atlet::class, 'users_id');
	}

	public function help_desks()
	{
		return $this->hasMany(HelpDesk::class, 'users_id');
	}

	public function help_desk_details()
	{
		return $this->hasMany(HelpDeskDetail::class, 'users_id');
	}

	public function klubs()
	{
		return $this->hasMany(Klub::class, 'users_id');
	}

	public function pengkots()
	{
		return $this->hasMany(Pengkot::class, 'users_id');
	}
}
