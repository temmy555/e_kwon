<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RolesModul
 * 
 * @property int $id
 * @property int $roles_id
 * @property int $modul_id
 * @property int $parent_id
 * @property int $is_hidden
 * 
 * @property Modul $modul
 * @property Role $role
 *
 * @package App\Models
 */
class RolesModul extends Model
{
	protected $table = 'roles_modul';
	public $timestamps = false;

	protected $casts = [
		'roles_id' => 'int',
		'modul_id' => 'int',
		'parent_id' => 'int',
		'is_hidden' => 'int'
	];

	protected $fillable = [
		'roles_id',
		'modul_id',
		'parent_id',
		'is_hidden'
	];

	public function modul()
	{
		return $this->belongsTo(Modul::class);
	}

	public function role()
	{
		return $this->belongsTo(Role::class, 'roles_id');
	}
}
