<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class HelpDeskDetail
 * 
 * @property int $id
 * @property string $isi
 * @property string $respon
 * @property Carbon $respon_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 * @property int $help_desk_id
 * @property int $users_id
 * 
 * @property HelpDesk $help_desk
 * @property User $user
 *
 * @package App\Models
 */
class HelpDeskDetail extends Model
{
	use SoftDeletes;
	protected $table = 'help_desk_detail';

	protected $casts = [
		'help_desk_id' => 'int',
		'users_id' => 'int'
	];

	protected $dates = [
		'respon_at'
	];

	protected $fillable = [
		'isi',
		'respon',
		'respon_at',
		'help_desk_id',
		'users_id'
	];

	public function help_desk()
	{
		return $this->belongsTo(HelpDesk::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class, 'users_id');
	}
}
