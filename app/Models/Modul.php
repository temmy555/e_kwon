<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Modul
 * 
 * @property int $id
 * @property string $modul
 * @property string $url
 * @property int $parent_id
 * @property int $is_hidden
 * @property string $icon_class
 * 
 * @property Collection|ModulAccess[] $modul_accesses
 * @property Collection|Role[] $roles
 *
 * @package App\Models
 */
class Modul extends Model
{
	protected $table = 'modul';
	public $timestamps = false;

	protected $casts = [
		'parent_id' => 'int',
		'is_hidden' => 'int'
	];

	protected $fillable = [
		'modul',
		'url',
		'parent_id',
		'is_hidden',
		'icon_class'
	];

	public function modul_accesses()
	{
		return $this->hasMany(ModulAccess::class);
	}

	public function roles()
	{
		return $this->belongsToMany(Role::class, 'roles_modul', 'modul_id', 'roles_id')
					->withPivot('id', 'parent_id', 'is_hidden');
	}
}
