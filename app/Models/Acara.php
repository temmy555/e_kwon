<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Acara
 * 
 * @property int $id
 * @property string $nama
 * @property string $lokasi
 * @property float $biaya_peratlet
 * @property Carbon $tgl_start
 * @property Carbon $tgl_end
 * @property Carbon $tgl_buka_pendaftara
 * @property Carbon $tgl_tutup pendaftaran
 * @property string $keterangan
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property Collection|BookingSlot[] $booking_slots
 * @property Collection|Kategori[] $kategoris
 *
 * @package App\Models
 */
class Acara extends Model
{
	use SoftDeletes;
	protected $table = 'acara';

	protected $casts = [
		'biaya_peratlet' => 'float'
	];

	protected $dates = [
		'tgl_start',
		'tgl_end',
		'tgl_buka_pendaftara',
		'tgl_tutup pendaftaran'
	];

	protected $fillable = [
		'nama',
		'lokasi',
		'biaya_peratlet',
		'tgl_start',
		'tgl_end',
		'tgl_buka_pendaftara',
		'tgl_tutup pendaftaran',
		'keterangan'
	];

	public function booking_slots()
	{
		return $this->hasMany(BookingSlot::class);
	}

	public function kategoris()
	{
		return $this->belongsToMany(Kategori::class, 'kategori_acara')
					->withPivot('id', 'atlet_id', 'kelas_id', 'juara_id', 'pengkot_id');
	}
}
