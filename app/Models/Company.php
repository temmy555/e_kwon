<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Company
 * 
 * @property int $id
 * @property string $cabang
 * @property string $nama
 * @property string $alamat
 * @property string $kota
 * @property string $logo
 *
 * @package App\Models
 */
class Company extends Model
{
	protected $table = 'company';
	public $timestamps = false;

	protected $fillable = [
		'cabang',
		'nama',
		'alamat',
		'kota',
		'logo'
	];
}
