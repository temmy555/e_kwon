<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Klub;
use App\Models\Pengkot;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Validator;
use Yajra\DataTables\DataTables;

class APengkotController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = Klub::with(['users', 'pengkot'])->find($uid);
            return view('admin.menus.master.pengkot.detail', compact('detail'));
        }
        $users = User::get();
        return view('admin.menus.master.pengkot.detail',compact('users'));
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $query = "select p.id, p.nama as pengkot, p.alamat, p.telpon, p.keterangan, s.nama as ketua from pengkot p
                        join users s on p.users_id = s.id
                        where p.deleted_at is null;";
            $result = DB::select($query);
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.master.pengkot.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'nama' => 'required',
            'ketua' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
        try {
            $uid = $inputs->input('id');
            $message = 'Pengkot Baru Berhasil Dibuat';

            $data = new Pengkot();
            if($uid){
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = Pengkot::find($uid);
                $message = 'Pengkot Berhasil Diedit';
            }
            $data->nama = $inputs->input('nama');
            $data->alamat = $inputs->input('alamat');
            $data->telpon = $inputs->input('telpon');
            $data->keterangan = $inputs->input('keterangan');
            $data->users_id = $inputs->input('ketua');
            $data->save();
            return Helper::redirect('pengkot.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        Pengkot::find($uid)->delete();

        return Helper::redirect('pengkot.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
        $timezone = 'ASIA/JAKARTA';
        $today = Carbon::today()->setTimezone($timezone)->toDateString();
        $model = Klub::query()->with(['users', 'pengkot']);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($data) use ($request) {
                $actions = "";
                $actions .= '<a href="javascript:void(0);" data-url="' . url('master/muatan_luar/del?id=' . $data->id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="Delete">
                        <i class="la la-trash"></i>
                    </a>';
                return $actions;
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }
}
