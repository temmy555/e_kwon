<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Atlet;
use App\Models\Modul;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;

class AAtletController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = Atlet::with(['users'])->find($uid);
            return view('admin.menus.master.muatan_luar.detail', compact('detail'));
        }
        return view('admin.menus.master.atlet.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $query = "select a.id,u.nama as nama, a.gender, kb.nama_klub as klub, s.geup as sabuk, kl.kategori_kelas as kategori, kl.kelas as kelas, a.tinggi, a.berat from atlet a
                        join users u on a.users_id = u.id
                        join kelas kl on a.kelas_id = kl.id
                        join klub kb on a.klub_id = kb.id
                        join sabuk s on a.sabuk_id = s.id
                        where a.deleted_at is null;";
            $result = DB::select($query);
//            $result = ModulAccess::with(['modul'])->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.master.atlet.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'penerima' => 'required',
            'jenis_biaya' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs;
        try {
            //generate no_trip
            $q = DataMuatan::where('no_trip', 'like', 'L%')
                ->latest()
                ->first();

            $new_code = '';
            $last = '';
            if ($q == null) {
                $new_code = 'L1';
            } else {
                // $last = Str::substr($hasil,2,1);
                $hasil = $q->no_trip;
                $last = (int)(Str::substr($hasil, 1, 5)) + 1;
                $new_code = 'L' . $last;
            }
            $uid = $inputs->input('id');
            $message = 'Data Muatan Luar Berhasil Dibuat';

            $data = new DataMuatan();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = DataMuatan::find($uid);
                $message = 'Data Muatan Luar Berhasil Diedit';
            }
            $data->no_trip = $new_code;
            $data->kode_data = 'MUATAN_LUAR';
            $data->penerima = $inputs->input('penerima');
            $data->jenis_biaya = $inputs->input('jenis_biaya');
            $data->penerima = $inputs->input('penerima');
            $tanggal = Carbon::createFromDate($inputs->input('tanggal'));;
            $data->tanggal = $tanggal;
            $data->penyewa = $inputs->input('penyewa');
            $data->muatan = $inputs->input('muatan');
            $data->no_pol = $inputs->input('no_pol');
            $data->jumlah = $inputs->input('jumlah');
            $data->nilai_awal = $inputs->input('nilai_awal');
            $data->asal = $inputs->input('asal');
            $data->tujuan = $inputs->input('tujuan');
            $data->users_id = auth()->id();
            $data->save();
            return Helper::redirect('muatan_luar.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        Atlet::find($uid)->delete();

        return Helper::redirect('muatan_luar.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
        $timezone = 'ASIA/JAKARTA';
        $today = Carbon::today()->setTimezone($timezone)->toDateString();
        $model = Atlet::query()->with(['users'])->where('kode_data', 'MUATAN_LUAR')->where('tanggal',$today);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($data) use ($request) {
                $actions = "";
                if (Helper::checkAccess($request, 'DELETE')) {
                    $actions .= '<a href="javascript:void(0);" data-url="' . url('master/muatan_luar/del?id=' . $data->id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="Delete">
                        <i class="la la-trash"></i>
                    </a>';
                }
                return $actions;
            })
            ->editColumn('tanggal', function ($data){
                return date('Y-m-d', strtotime($data->tanggal) );
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }
}
