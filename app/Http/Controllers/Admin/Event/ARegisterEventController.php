<?php

namespace App\Http\Controllers\Admin\Event;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Klub;
use App\Models\Pengkot;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Validator;
use Yajra\DataTables\DataTables;

class ARegisterEventController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        $users = User::get();
        $pengkot = Pengkot::get();
        if ($uid) {
            $detail = Klub::with(['users', 'pengkot'])->find($uid);
            return view('admin.menus.event.participant.detail', compact('detail'));
        }
        return view('admin.menus.event.participant.detail',compact('users','pengkot'));
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $query = "select ep.id, a2.nama as event, u.nama as nama, k2.nama as tim, k3.nama as kategori, concat(k.gender,k.kelas) as kelas from event_participants ep
                        join atlet a on ep.atlet_id = a.id
                            join users u on a.users_id = u.id
                        join booking_slot bs on ep.booking_slot_id = bs.id
                            join acara a2 on bs.acara_id = a2.id
                            join klub k2 on bs.klub_id = k2.id
                        join kelas k on ep.kelas_id = k.id
                        join kategori k3 on k.kategori_id = k3.id
                        where ep.deleted_at is null;";
            $result = DB::select($query);
//            $result = ModulAccess::with(['modul'])->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.event.participant.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'penerima' => 'required',
            'jenis_biaya' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs;
        try {
            //generate no_trip
            $q = DataMuatan::where('no_trip', 'like', 'L%')
                ->latest()
                ->first();

            $new_code = '';
            $last = '';
            if ($q == null) {
                $new_code = 'L1';
            } else {
                // $last = Str::substr($hasil,2,1);
                $hasil = $q->no_trip;
                $last = (int)(Str::substr($hasil, 1, 5)) + 1;
                $new_code = 'L' . $last;
            }
            $uid = $inputs->input('id');
            $message = 'Data Muatan Luar Berhasil Dibuat';

            $data = new DataMuatan();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = DataMuatan::find($uid);
                $message = 'Data Muatan Luar Berhasil Diedit';
            }
            $data->no_trip = $new_code;
            $data->kode_data = 'MUATAN_LUAR';
            $data->penerima = $inputs->input('penerima');
            $data->jenis_biaya = $inputs->input('jenis_biaya');
            $data->penerima = $inputs->input('penerima');
            $tanggal = Carbon::createFromDate($inputs->input('tanggal'));;
            $data->tanggal = $tanggal;
            $data->penyewa = $inputs->input('penyewa');
            $data->muatan = $inputs->input('muatan');
            $data->no_pol = $inputs->input('no_pol');
            $data->jumlah = $inputs->input('jumlah');
            $data->nilai_awal = $inputs->input('nilai_awal');
            $data->asal = $inputs->input('asal');
            $data->tujuan = $inputs->input('tujuan');
            $data->users_id = auth()->id();
            $data->save();
            return Helper::redirect('muatan_luar.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

//        $invalid = $this->isInvalid($uid);
//        if ($invalid) {
//            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
//        }
//        return $uid;

        Klub::find($uid)->delete();

        return Helper::redirect('klub.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public
    function dataTable(Request $request)
    {
        $timezone = 'ASIA/JAKARTA';
        $today = Carbon::today()->setTimezone($timezone)->toDateString();
        $model = Klub::query()->with(['users', 'pengkot']);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($data) use ($request) {
                $actions = "";
                $actions .= '<a href="javascript:void(0);" data-url="' . url('master/muatan_luar/del?id=' . $data->id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="Delete">
                        <i class="la la-trash"></i>
                    </a>';
                return $actions;
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }
}
