<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Acara;

class FHomeController extends Controller
{
    public function index()
    {
//        $query2 = "select p.id, kh.no_kwitansi, kh.no_trip ,kh.kepada,sum(oc.jumlah) as jumlah, (kh.total+kh.ppn) as tagihan ,p.approve, u.username, GROUP_CONCAT(
//                            DISTINCT oc.description ORDER BY oc.description SEPARATOR ' | '
//                        ) as no_crane
//                    from users u
//                    join kwitansi_print p on p.pembuat = u.id
//                    join kwitansi_header kh on p.header_kwitansi_id = kh.id
//                    join kwitansi_detail d on d.header_kwitansi_id = kh.id
//                    join order_crane oc on d.order_id = oc.id
//                    group by p.id, kh.no_kwitansi, kh.no_trip, kh.kepada, p.approve,u.username, tagihan
//                    order by p.id desc;";
//        $data = Helper::selectMany($query2);
        $acara = Acara::get();
        return view('frontend.menus.home.index',compact('acara'));
    }
}
