<?php

namespace App\Providers;

use App\Helpers\Helper;
use App\Models\BeoneKonfigurasiPerusahaan;
use App\Models\BeoneUser;
use App\Models\Company;
use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        view()->composer('*', function ($view) {
            if (Auth::check()) {
                $user = User::with(['role.moduls'])->find(Auth::id());
                $g_settings = Company::first();
                $g_moduls = $user->role->moduls;
                $g_sidebar = Helper::sidebar(request(), collect($g_moduls)->toArray());
                $view->with('g_sidebar', $g_sidebar);
                $view->with('g_moduls', $g_moduls);
                $view->with('g_settings', $g_settings);
            }
        });
    }
}
