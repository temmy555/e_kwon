<?php

namespace Vendor\Package\Stimulsoft\Classes;

class StiResponse
{
    public static function json($result, $exit = true)
    {
        unset($result->object);
        if (defined('JSON_UNESCAPED_SLASHES')) echo json_encode($result, JSON_UNESCAPED_SLASHES);
        else echo json_encode($result);
        if ($exit) exit;
    }
}
