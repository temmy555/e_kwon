<?php
Route::group(['middleware' => ['web']], function () {
    Route::get('front', 'Frontend\FHomeController@index');
    Route::get('login', 'Admin\Auth\ALoginController@index')->name('login');
    Route::get('logout', 'Admin\Auth\ALoginController@logout')->name('logout');
    Route::post('login', 'Admin\Auth\ALoginController@authenticate');

    Route::group(['middleware' => ['auth']], function () {
        Route::get('/', 'Admin\Dashboard\ADashboardController@index')->name('home');
        Route::group(['prefix' => 'master'], function () {
            Route::group(['prefix' => 'klub'], function () {
                Route::get('del', 'Admin\Master\AKlubController@deleteData')->name('klub.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Master\AKlubController@indexDetail')->name('klub.detail');
                    Route::post('/', 'Admin\Master\AKlubController@postDetail')->name('klub.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\AKlubController@indexList')->name('klub.list');
                });
            });
            Route::group(['prefix' => 'pengkot'], function () {
                Route::get('del', 'Admin\Master\APengkotController@deleteData')->name('pengkot.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Master\APengkotController@indexDetail')->name('pengkot.detail');
                    Route::post('/', 'Admin\Master\APengkotController@postDetail')->name('pengkot.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\APengkotController@indexList')->name('pengkot.list');
                });
            });
            Route::group(['prefix' => 'sabuk'], function () {
                Route::get('del', 'Admin\Master\ASabukController@deleteData')->name('sabuk.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Master\ASabukController@indexDetail')->name('sabuk.detail');
                    Route::post('/', 'Admin\Master\ASabukController@postDetail')->name('sabuk.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\ASabukController@indexList')->name('sabuk.list');
                });
            });
            Route::group(['prefix' => 'kategori'], function () {
                Route::get('del', 'Admin\Master\AKategoriController@deleteData')->name('kategori.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Master\AKategoriController@indexDetail')->name('kategori.detail');
                    Route::post('/', 'Admin\Master\AKategoriController@postDetail')->name('kategori.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\AKategoriController@indexList')->name('kategori.list');
                });
            });
            Route::group(['prefix' => 'kelas'], function () {
                Route::get('del', 'Admin\Master\AKelasController@deleteData')->name('kelas.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Master\AKelasController@indexDetail')->name('kelas.detail');
                    Route::post('/', 'Admin\Master\AKelasController@postDetail')->name('kelas.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\AKelasController@indexList')->name('kelas.list');
                });
            });
            Route::group(['prefix' => 'atlet'], function () {
                Route::get('del', 'Admin\Master\AAtletController@deleteData')->name('atlet.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Master\AAtletController@indexDetail')->name('atlet.detail');
                    Route::post('/', 'Admin\Master\AAtletController@postDetail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Master\AAtletController@indexList')->name('atlet.list');
                });
            });
        });
        Route::group(['prefix' => 'event'], function () {
            Route::group(['prefix' => 'acara'], function () {
                Route::get('del', 'Admin\Event\AAcaraController@deleteData')->name('acara.delete');
                Route::get('peserta', 'Admin\Event\AAcaraController@showPeserta')->name('acara.peserta');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Event\AAcaraController@indexDetail')->name('acara.detail');
                    Route::post('/', 'Admin\Event\AAcaraController@postDetail')->name('acara.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Event\AAcaraController@indexList')->name('acara.list');
                });
            });
            Route::group(['prefix' => 'booking'], function () {
                Route::get('del', 'Admin\Event\ABookingController@deleteData')->name('booking.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Event\ABookingController@indexDetail')->name('booking.detail');
                    Route::post('/', 'Admin\Event\ABookingController@postDetail')->name('booking.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Event\ABookingController@indexList')->name('booking.list');
                });
            });
            Route::group(['prefix' => 'participant'], function () {
                Route::get('del', 'Admin\Event\ARegisterEventController@deleteData')->name('participant.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Event\ARegisterEventController@indexDetail')->name('participant.detail');
                    Route::post('/', 'Admin\Event\ARegisterEventController@postDetail')->name('participant.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Event\ARegisterEventController@indexList')->name('participant.list');
                });
            });
        });
        Route::group(['prefix' => 'olap'], function () {
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\OLAP\AOLAPController@indexDetail')->name('olap.detail');
                Route::post('/', 'Admin\OLAP\AOLAPController@postDetail')->name('olap.detail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\OLAP\AOLAPController@indexList')->name('olap.list');
            });
            Route::group(['prefix' => 'print_out'], function () {
                Route::get('data', 'Admin\OLAP\APrintOutController@getData')->name('olap.print_out.data');
                Route::group(['prefix' => 'view'], function () {
                    Route::get('/', 'Admin\OLAP\APrintOutController@index')->name('olap.print_out.view');
                    Route::post('/', 'Admin\OLAP\APrintOutController@index');
                });
            });
        });
        Route::group(['prefix' => 'sistem'], function () {
            Route::group(['prefix' => 'user'], function () {
                Route::get('del', 'Admin\Sistem\ASistemUserController@deleteData')->name('user.delete');
                Route::get('ganti-password', 'Admin\Sistem\ASistemUserController@ganti_password')->name('password.reset');
                Route::post('update', 'Admin\Sistem\ASistemUserController@updatePassword')->name('password.update');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Sistem\ASistemUserController@indexDetail')->name('user.detail');
                    Route::post('/', 'Admin\Sistem\ASistemUserController@postDetail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Sistem\ASistemUserController@indexList')->name('user.list');
                });
            });
            Route::group(['prefix' => 'role_user'], function () {
                Route::get('s2', 'Admin\Sistem\ASistemRoleUserController@searchData')->name('s2.role_user');
                Route::get('del', 'Admin\Sistem\ASistemRoleUserController@deleteData')->name('role_user.delete');
                Route::group(['prefix' => 'detail'], function () {
                    Route::get('/', 'Admin\Sistem\ASistemRoleUserController@indexDetail')->name('role_user.detail');
                    Route::post('/', 'Admin\Sistem\ASistemRoleUserController@postDetail')->name('role_user.detail');
                });
                Route::group(['prefix' => 'list'], function () {
                    Route::get('/', 'Admin\Sistem\ASistemRoleUserController@indexList')->name('role_user.list');
                });
            });
        });
    });
});
