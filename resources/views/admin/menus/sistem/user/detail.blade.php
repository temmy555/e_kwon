@extends('admin.layouts.default')
@section('title', $title='Sistem User')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs form-submit" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="id" value="{{$detail->id}}"/>
                            @endisset
                            <div class="form-group">
                                <label>Nama:</label>
                                <input type="text" name="nama" class="form-control" placeholder="Nama"
                                       value="{{old('nama',(isset($detail)? $detail->nama : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Username:</label>
                                <input type="text" name="username" class="form-control" placeholder="Username"
                                       value="{{old('username',(isset($detail)? $detail->username : ''))}}"
                                       required>
                            </div>
                            <div class="form-group">
                                <label>Password:</label>
                                <input type="password" name="password" id="password" class="form-control"
                                       placeholder="{{(isset($detail->id)? 'Kosongi jika tidak mengganti password.' : 'Password')}}"
                                    {{(isset($detail->id)? '' : 'required')}}
                                >
                            </div>
                            <div class="form-group">
                                <label>Validasi Password:</label>
                                <input type="password" name="validasi_password" id="validasi_password"
                                       class="form-control"
                                       placeholder="{{(isset($detail->id)? 'Kosongi jika tidak mengganti password.' : 'Validasi Password')}}"
                                       data-parsley-equalto="#password"
                                    {{(isset($detail->id)? '' : 'required')}}
                                >
                            </div>
                            <div class="form-group">
                                <label>Role</label>
                                <div class="input-group">
                                    <select class="form-control kt-select2"
                                            name="role_id"
                                            required>
                                        <option></option>
                                        @foreach ($role as $r)
                                            <option value="{{$r->id}}" {{(old('role_id', isset($detail->role_id) ? $detail->role_id : '' ) == $r->id ? 'selected':'') }}>{{$r->nama_role}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-12 ml-lg-auto">
                                        <button type="submit" class="btn btn-brand">Submit</button>
                                        <a data-url="{{url()->previous()}}"
                                           class="btn btn-secondary prevent-dialog"
                                           data-sw-title="Yakin Cancel?">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $('#password, #validasi_password').on('keyup', function () {
            if ($('#password').val() == $('#validasi_password').val()) {
                $('#message').html('Password Matching').css('color', 'green');
            } else
                $('#message').html('Password Not Matching').css('color', 'red');
        });
    </script>
@endpush
