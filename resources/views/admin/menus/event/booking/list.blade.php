@extends('admin.layouts.default')
@section('title', $title='Booking')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        @include('admin.includes.alert')
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-wrapper">
                                <div class="kt-portlet__head-actions">
                                    <a href="{{route('booking.detail')}}"
                                       class="btn btn-success btn-elevate btn-icon-sm">
                                        <i class="la la-plus"></i>
                                        Buat Baru
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="gridContainer"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(function () {
            thisForm.init();
        }), thisForm = {
            init: function () {
                thisForm.dataGrid();
            },
            dataGrid: function () {
                $access = $("#gridContainer").kendoGrid({
                        dataSource: {
                            transport: {
                                read: {
                                    data: {id: '{{(isset($detail)? $detail->id : 0)}}'},
                                    url: "{{url()->current()}}",
                                    dataType: "jsonp",
                                }
                            },
                            schema: {
                                model: {
                                    fields: {
                                        id: {type: "id"},
                                        biaya_total: {type: "number"},
                                    }
                                }
                            },
                            group: {field: "acara"}
                        },
                        // selectable: "multiple",
                        filterable: true,
                        columnMenu: true,
                        sortable: true,
                        resizable: true,
                        reorderable: true,
                        groupable: true,
                        persistSelection: true,
                        dataBound: onDataBound,
                        pageable: {
                            pageSize: 10
                        },
                        columns: [
                            {field: "no_invoice", title: "No. Invoice"},
                            {field: "tim", title: "Tim"},
                            {field: "acara", title: "Event", hidden: true},
                            {field: "jml_slot", title: "Booking", width: 120},
                            {field: "biaya_total", title: "Total Tagihan", width: 150, template: "#= currency(data)#"},
                            {field: "verified", title: "Verifikasi", width: 120},
                            {
                                title: "Action",
                                width: 100,
                                template: "<button role='button' type='button' class='k-button k-button-icontext k-grid-edit delete-data'><span class='k-icon la la-trash'></span>Delete</button>"
                            }
                        ]
                    }
                ).data("kendoGrid");
            }
        }
        ;

        function onDataBound(e) {
            const grid = this;
            const rows = grid.items();

            $(rows).each(function (e) {
                const row = this;
                const dataItem = grid.dataItem(row);
                if (dataItem.granted > 0) {
                    grid.select(row);
                }
            });
            $("#gridContainer .delete-data").on('click', function () {
                const dataItem = grid.dataItem($(this).closest('tr'));
                $link = "{{route('klub.delete')}}" + "?id=" + dataItem.id;
                // console.log(dataItem);
                // location.href = $link; //ini langsung redirect
                swal.fire({
                    title: 'Are you sure want to delete ?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak'
                }).then(function (result) {
                    if (result.value) {
                        location.href = $link;
                    }
                });
            });
        }

        function currency(data) {
            if (data.biaya_total > 1000) {
                return kendo.toString(data.biaya_total, "##,#")
            }
            return kendo.toString(data.biaya_total, "##,#")
        }
    </script>
@endpush
