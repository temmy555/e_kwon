@extends('admin.layouts.default')
@section('title', $title='Pengkot')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="id" value="{{$detail->id}}"/>
                            @endisset
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Pengurus Kota:</label>
                                        <input type="text" onkeyup="this.value = this.value.toUpperCase();"
                                               name="nama"
                                               class="form-control" placeholder="Masukkan Kota"
                                               value="{{old('nama',(isset($detail)? $detail->nama : ''))}}"
                                               required>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Ketua:</label>
                                        <div class="input-group">
                                            <select class="form-control kt-select2"
                                                    name="ketua"
                                                    required>
                                                <option></option>
                                                @foreach($users as $u)
                                                    <option value="{{$u->id}}">{{$u->nama}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Telephone:</label>
                                        <input type="number"
                                               name="telpon" placeholder="Masukkan telephone"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Alamat Kantor:</label>
                                        <textarea type="text"
                                                  onkeyup="this.value = this.value.toUpperCase();"
                                                  class="form-control" name="alamat"
                                                  placeholder="Masukkan Alamat"
                                                  value="{{old('alamat',(isset($detail)? $detail->alamat : ''))}}"></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Keterangan:</label>
                                        <textarea type="text"
                                                  onkeyup="this.value = this.value.toUpperCase();"
                                                  class="form-control" name="keterangan"
                                                  placeholder="Masukkan Deskripsi"
                                                  value="{{old('keterangan',(isset($detail)? $detail->keterangan : ''))}}"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <div class="row">
                                        <div class="col-lg-12 ml-lg-auto">
                                            <button type="submit" class="btn btn-brand">Submit</button>
                                            <a data-url="{{url()->previous()}}"
                                               class="btn btn-secondary prevent-dialog"
                                               data-sw-title="Yakin Cancel?">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
@endpush
