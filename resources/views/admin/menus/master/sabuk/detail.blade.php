@extends('admin.layouts.default')
@section('title', $title='Geup')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="id" value="{{$detail->id}}"/>
                            @endisset
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Geup:</label>
                                        <input type="text" onkeyup="this.value = this.value.toUpperCase();"
                                               name="geup"
                                               class="form-control" placeholder="Masukkan Nama Tim"
                                               value="{{old('geup',(isset($detail)? $detail->geup : ''))}}"
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Keterangan:</label>
                                        <textarea type="text"
                                                  onkeyup="this.value = this.value.toUpperCase();"
                                                  class="form-control" name="keterangan"
                                                  placeholder="Masukkan Deskripsi"
                                                  value="{{old('keterangan',(isset($detail)? $detail->keterangan : ''))}}"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <div class="row">
                                        <div class="col-lg-12 ml-lg-auto">
                                            <button type="submit" class="btn btn-brand">Submit</button>
                                            <a data-url="{{url()->previous()}}"
                                               class="btn btn-secondary prevent-dialog"
                                               data-sw-title="Yakin Cancel?">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
@endpush
