@extends('admin.layouts.default')
@section('title', $title='TIM')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{$title}}
                            </h3>
                        </div>
                    </div>
                    <form class="kt-form kt-form--label-right form-validatejs" method="post"
                          action="{{url()->current()}}">
                        @csrf
                        <div class="kt-portlet__body">
                            @include('admin.includes.alert')
                            @isset($detail)
                                <input type="hidden" name="id" value="{{$detail->id}}"/>
                            @endisset
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Nama Tim:</label>
                                        <input type="text" onkeyup="this.value = this.value.toUpperCase();"
                                               name="nama_klub"
                                               class="form-control" placeholder="Masukkan Nama Tim"
                                               value="{{old('nama_klub',(isset($detail)? $detail->nama_klub : ''))}}"
                                               required>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Manager</label>
                                        <div class="input-group">
                                            <select class="form-control kt-select2"
                                                    name="manager"
                                                    required>
                                                <option></option>
                                                @foreach($users as $u)
                                                    <option value="{{$u->id}}">{{$u->nama}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Pengkot</label>
                                        <div class="input-group">
                                            <select class="form-control kt-select2"
                                                    name="pengkot"
                                                    required>
                                                <option></option>
                                                @foreach($pengkot as $p)
                                                    <option value="{{$p->id}}">{{$p->nama}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>Telephone:</label>
                                        <input type="number"
                                               name="telpon" placeholder="Masukkan telephone"
                                               class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Alamat:</label>
                                        <input type="text" onkeyup="this.value = this.value.toUpperCase();"
                                               name="alamat"
                                               class="form-control" placeholder="Masukkan Alamat"
                                               value="{{old('alamat',(isset($detail)? $detail->alamat : ''))}}"
                                               required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Keterangan:</label>
                                        <input type="text" onkeyup="this.value = this.value.toUpperCase();"
                                               name="keterangan"
                                               class="form-control" placeholder="Keterangan"
                                               value="{{old('keterangan',(isset($detail)? $detail->keterangan : ''))}}">
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                    <div class="row">
                                        <div class="col-lg-12 ml-lg-auto">
                                            <button type="submit" class="btn btn-brand">Submit</button>
                                            <a data-url="{{url()->previous()}}"
                                               class="btn btn-secondary prevent-dialog"
                                               data-sw-title="Yakin Cancel?">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
@endpush
