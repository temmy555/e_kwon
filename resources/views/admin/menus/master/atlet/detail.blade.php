@extends('admin.layouts.default')
@section('title', $title='New Atlet')
@section('content')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet">
            <div class="kt-portlet__body">
                @include('admin.includes.alert')
                @isset($detail)
                    <input type="hidden" name="id" value="{{$detail->id}}"/>
                @endisset
                <div class="kt-grid">
                    <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v4__wrapper">
                        <form class="kt-form kt-form--label-right form-validatejs" method="post"
                              action="{{url()->current()}}">
                            @csrf
                            <div class="kt-wizard-v4__content" data-ktwizard-type="step-content"
                                 data-ktwizard-state="current">
                                <div class="kt-heading kt-heading--md">Atlet's Profile Details:</div>
                                <div class="kt-section kt-section--first">
                                    <div class="kt-wizard-v4__form">
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <div class="kt-section__body">
                                                    <div class="form-group row">
                                                        <label
                                                            class="col-xl-3 col-lg-3 col-form-label">Avatar</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class="kt-avatar kt-avatar--outline"
                                                                 id="kt_user_add_avatar">
                                                                <div class="kt-avatar__holder"
                                                                     style="background-image: url(/images/avatar.png)">
                                                                    <label class="kt-avatar__upload"
                                                                           data-toggle="kt-tooltip" title
                                                                           data-original-title="Change avatar">
                                                                        <i class="fa fa-pen"></i>
                                                                        <input type="file"
                                                                               name="kt_user_add_user_avatar">
                                                                    </label>
                                                                    <span class="kt-avatar__cancel"
                                                                          data-toggle="kt-tooltip" title
                                                                          data-original-title="Cancel avatar">
                                                                                <i class="fa fa-times"></i>
                                                                            </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">Full
                                                            Name</label>
                                                        <div class="col-lg-9 col-xl-6">

                                                            <div class="col-lg-9 col-xl-9">
                                                                <input class="form-control" type="text"
                                                                       value="Anna Krox" aria-invalid="false">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label
                                                            class="col-xl-3 col-lg-3 col-form-label">Birthday</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class="col-lg-9 col-xl-9">
                                                                <div class="input-group date">
                                                                    <input type="text"
                                                                           class="form-control date-picker"
                                                                           name="tanggal"
                                                                           value="{{old('tanggal',(isset($detail)? $detail->tanggal : ''))}}"
                                                                           placeholder="Select date"
                                                                        {{(isset($detail)? 'disabled="disabled"' : '')}}
                                                                    />
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text">
                                                                            <i class="la la-calendar-check-o"></i>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label
                                                            class="col-xl-3 col-lg-3 col-form-label">Club</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class="col-lg-9 col-xl-9">
                                                                <select class="form-control"
                                                                        aria-invalid="false">
                                                                    <option>Select Club</option>
                                                                    <option>TTM</option>
                                                                    <option>Zhiendo</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label
                                                            class="col-xl-3 col-lg-3 col-form-label">Geup</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class="col-lg-9 col-xl-9">
                                                                <select class="form-control"
                                                                        aria-invalid="false">
                                                                    <option>Select Geup</option>
                                                                    <option>GEUP 10</option>
                                                                    <option>GEUP 9</option>
                                                                    <option>DAN I</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label
                                                            class="col-xl-3 col-lg-3 col-form-label">Category</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class="col-lg-9 col-xl-9">
                                                                <select class="form-control"
                                                                        aria-invalid="false">
                                                                    <option>Select Category</option>
                                                                    <option>SENIOR</option>
                                                                    <option>JUNIOR</option>
                                                                    <option>CADET</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label
                                                            class="col-xl-3 col-lg-3 col-form-label">Gender</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class="col-lg-9 col-xl-9">
                                                                <select class="form-control"
                                                                        aria-invalid="false">
                                                                    <option>Select Gender</option>
                                                                    <option>MALE</option>
                                                                    <option>FEMALE</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label
                                                            class="col-xl-3 col-lg-3 col-form-label">Height</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class="col-lg-9 col-xl-9">
                                                                <input class="form-control" type="number"
                                                                       value="170" aria-invalid="false">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label
                                                            class="col-xl-3 col-lg-3 col-form-label">Weight</label>
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class="col-lg-9 col-xl-9">
                                                                <input class="form-control" type="number"
                                                                       value="65" aria-invalid="false">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
@endpush
