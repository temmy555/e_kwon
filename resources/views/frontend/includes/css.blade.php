<link href="{{ asset('css/front.css') }}" rel="stylesheet" type="text/css"/>
<style>
  #header.header-colored-transparent+#google-map,
  #header.header-colored-transparent+#page-title:not(.page-title-classic),
  #header.header-colored-transparent+#slider,
  #header.header-colored-transparent+.fullscreen,
  #header.header-colored-transparent+.halfscreen,
  #header.header-dark-transparent+#google-map,
  #header.header-dark-transparent+#page-title:not(.page-title-classic),
  #header.header-dark-transparent+#slider,
  #header.header-dark-transparent+.fullscreen,
  #header.header-dark-transparent+.halfscreen,
  #header.header-light-transparent+#google-map,
  #header.header-light-transparent+#page-title:not(.page-title-classic),
  #header.header-light-transparent+#slider,
  #header.header-light-transparent+.fullscreen,
  #header.header-light-transparent+.halfscreen,
  #header.header-transparent+#google-map,
  #header.header-transparent+#page-title:not(.page-title-classic),
  #header.header-transparent+#slider,
  #header.header-transparent+.fullscreen,
  #header.header-transparent+.halfscreen {
    top: -100px;
    margin-bottom: -80px
  }
  #header {
      width: 100%;
      z-index: 199!important;
      height: 100px !important;
      line-height: 80px;
  }
  #header .container {
    padding-top: 10px !important;
    padding-bottom: 10px !important;
  }
  #header .logo a {
    max-width: 80px;
    max-height: 80px;
  }
</style>
