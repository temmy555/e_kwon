<header id="header" class="header-transparent dark">
   <div id="header-wrap">
      <div class="container">
         <div id="logo">
            <a href="index.html" class="logo" data-dark-logo="{{asset('global/logo_tkd.png')}}">
              <img src="{{asset('global/logo_tkd.png')}}" alt="Tkd Logo">
            </a>
         </div>
         <div id="top-search">
            <form action="http://www.inspirothemes.com/polo-v4/search-results-page.html" method="get">
               <input type="text" name="q" class="form-control" value="" placeholder="Start typing & press  &quot;Enter&quot;">
            </form>
         </div>
         <!-- <div class="header-extras">
            <ul>

            </ul>
         </div> -->
         <div id="mainMenu-trigger">
            <button class="lines-button x"> <span class="lines"></span> </button>
         </div>
         <div id="mainMenu" class="light">
            <div class="container">
               <nav>
                  <ul>
                     <li><a href="index.html">Beranda</a></li>
                     <li><a href="index.html">Info Kejuaraan</a></li>
                     <li><a href="index.html">Pendaftaran Kejuaraan</a></li>
                     <li><a href="index.html">Pendaftaran UKT</a></li>
                     <li><a href="index.html">LOGIN</a></li>
                  </ul>
               </nav>
            </div>
         </div>
      </div>
   </div>
</header>
