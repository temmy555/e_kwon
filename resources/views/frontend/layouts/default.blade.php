<!DOCTYPE html>
<html lang="en">
   <head>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta http-equiv="content-type" content="text/html; charset=utf-8" />
      <meta name="author" content="INSPIRO" />
      <meta name="description" content="Themeforest Template Polo">
      <title>Taekwondo | PengprovTI Jatim</title>
      @include('frontend.includes.css')
   </head>
   <body>
     <div id="wrapper">
        @include('frontend.includes.header')
        @include('frontend.includes.slider')
        @yield('content')
        @include('frontend.includes.footer')
     </div>
     <a id="goToTop"><i class="fa fa-angle-up top-icon"></i><i class="fa fa-angle-up"></i></a>
     @include('frontend.includes.script')
   </body>
</html>
