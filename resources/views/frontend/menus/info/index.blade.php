@extends('frontend.layouts.info')
@section('title','Info Kejuaraan')
@section('contentinfo')
<section class="background-grey">
   <div class="container">
      <div class="heading text-left">
         <h2>INFOOO PEMENANG</h2>
         <span class="lead">Create amam ipsum dolor sit amet, consectetur adipiscing elit.</span>
      </div>
      <div class="row">
         <div class="col-md-4">
            <div class="" data-animation="fadeInUp" data-animation-delay="0">
               <h4>Modern Design</h4>
               <p>Lorem ipsum dolor sit amet, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis aliquam.</p>
            </div>
         </div>
         <div class="col-md-4">
            <div class="" data-animation="fadeInUp" data-animation-delay="200">
               <h4>Loaded with Features</h4>
               <p>Lorem ipsum dolor sit amet, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis aliquam.</p>
            </div>
         </div>
         <div class="col-md-4">
            <div class="" data-animation="fadeInUp" data-animation-delay="400">
               <h4>Completely Customizable</h4>
               <p>Lorem ipsum dolor sit amet, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis aliquam.</p>
            </div>
         </div>
         <div class="col-md-4">
            <div class="" data-animation="fadeInUp" data-animation-delay="600">
               <h4>100% Responsive Layout</h4>
               <p>Lorem ipsum dolor sit amet, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis aliquam.</p>
            </div>
         </div>
         <div class="col-md-4">
            <div class="" data-animation="fadeInUp" data-animation-delay="800">
               <h4>Clean Modern Code</h4>
               <p>Lorem ipsum dolor sit amet, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis aliquam.</p>
            </div>
         </div>
         <div class="col-md-4">
            <div class="" data-animation="fadeInUp" data-animation-delay="1000">
               <h4>Free Updates & Support</h4>
               <p>Lorem ipsum dolor sit amet, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis aliquam.</p>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection
