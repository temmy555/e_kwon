@extends('frontend.layouts.register')
@section('title','Register')
@section('content')

<section class="background-grey">
   <div class="container">
      <div class="col-md-6">
         <h2>Pendaftaran Kejuaraan</h2>
         <span class="lead">Masukkan ID Atlet Anda : </span><br>
         <input type="text" class="form-control" placeholder="ID ATLET" /><br>
         <span class="lead">Pilih Kejuaraan Yang Diikuti : </span><br>
         <select class="form-control show-tick ms select2" data-placeholder="Select">
             <option></option>
             <option>Mustard</option>
             <option>Ketchup</option>
             <option>Relish</option>
         </select><br>
         <button type="button" class="btn btn-raised btn-primary btn-round waves-effect">DAFTAR</button>
      </div>
   </div>
</section>

@endsection
