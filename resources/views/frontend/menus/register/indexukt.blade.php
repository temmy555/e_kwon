@extends('frontend.layouts.register')
@section('title','Register')
@section('content')

<section class="background-grey">
   <div class="container">
      <div class="body_scroll">
         <h2>Pendaftaran UKT (Ujian Kenaikan Tingkat)</h2>
            <div class="col-md-6">
                <span class="lead">Masukkan ID Atlet Anda : </span><br>
                <input type="text" class="form-control" placeholder="ID ATLET" /><br>
                <span class="lead">Pilih Event UKT : </span><br>
                <select class="form-control show-tick ms select2" data-placeholder="Select">
                  <option></option>
                  <option>Mustard</option>
                  <option>Ketchup</option>
                  <option>Relish</option>
                </select><br>
                <button type="button" class="btn btn-raised btn-primary btn-round waves-effect">DAFTAR</button>
            </div>
      </div>
   </div>
</section>

@endsection
