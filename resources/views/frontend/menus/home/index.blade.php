@extends('frontend.layouts.default')
@section('title','MainMenu')
@section('content')

    <section class="p-b-0">
        <div class="container">
            <div class="heading heading-center m-b-40" data-animation="fadeInUp">
                <h2>SELAMAT DATANG DI PENGPROVTI JATIM</h2>
                <span class="lead">Create amam ipsum dolor sit amet, consectetur adipiscing elit.</span>
            </div>
            {{--       <div class="row" data-animation="fadeInUp">--}}
            {{--         <div class="col-md-12">--}}
            {{--            <img class="img-responsive" src="{{asset('global/images/other/responsive-1.jpg')}}" alt="Welcome to POLO">--}}
            {{--         </div>--}}
            {{--      </div>--}}
        </div>
    </section>
    <section class="background-grey">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 center text-center">
                    <h4 class="m-b-10">Know More</h4>
                    <h2>About Our Organization</h2>
                    <p>Create amam ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="" data-animation="fadeInUp" data-animation-delay="0">
                        <h4 class="m-b-20">DAFTAR KEJUARAAN 2020</h4>
                        <div class="accordion fancy radius clean">
                            @foreach($acara as $a)
                            <div class="ac-item">
                                <h5 class="ac-title">
                                    <i class="fa fa-calendar"></i>
                                    {{$a->nama}}
                                </h5>
                                <div class="ac-content ac-active">
                                    Lokasi : {{$a->lokasi}}
                                    <br>
                                    Tanggal : {{date('d-m-Y',strtotime($a->tgl_start))}} - {{date('d-m-Y',strtotime($a->tgl_end))}}
                                    <br>
                                    Biaya Pendaftaran : Rp. {{ number_format($a->biaya_peratlet,'0', '.', ',') }}
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="" data-animation="fadeInUp" data-animation-delay="200">
                        <h4>Daftar Pemenang</h4>
                        <p>Lorem ipsum dolor sit amet, blandit vel sapien vitae, condimentum ultricies magna et. Quisque
                            euismod orci ut et lobortis aliquam.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="" data-animation="fadeInUp" data-animation-delay="0">
                        <h4>Modern Design</h4>
                        <p>Lorem ipsum dolor sit amet, blandit vel sapien vitae, condimentum ultricies magna et. Quisque
                            euismod orci ut et lobortis aliquam.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="" data-animation="fadeInUp" data-animation-delay="200">
                        <h4>Loaded with Features</h4>
                        <p>Lorem ipsum dolor sit amet, blandit vel sapien vitae, condimentum ultricies magna et. Quisque
                            euismod orci ut et lobortis aliquam.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="" data-animation="fadeInUp" data-animation-delay="400">
                        <h4>Completely Customizable</h4>
                        <p>Lorem ipsum dolor sit amet, blandit vel sapien vitae, condimentum ultricies magna et. Quisque
                            euismod orci ut et lobortis aliquam.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="" data-animation="fadeInUp" data-animation-delay="600">
                        <h4>100% Responsive Layout</h4>
                        <p>Lorem ipsum dolor sit amet, blandit vel sapien vitae, condimentum ultricies magna et. Quisque
                            euismod orci ut et lobortis aliquam.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="" data-animation="fadeInUp" data-animation-delay="800">
                        <h4>Clean Modern Code</h4>
                        <p>Lorem ipsum dolor sit amet, blandit vel sapien vitae, condimentum ultricies magna et. Quisque
                            euismod orci ut et lobortis aliquam.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="" data-animation="fadeInUp" data-animation-delay="1000">
                        <h4>Free Updates & Support</h4>
                        <p>Lorem ipsum dolor sit amet, blandit vel sapien vitae, condimentum ultricies magna et. Quisque
                            euismod orci ut et lobortis aliquam.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
