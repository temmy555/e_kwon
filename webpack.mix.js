const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

// copy images folder into laravel public folder
mix.copyDirectory('resources/demo1/src/assets/media', 'public/assets/media');
mix.copyDirectory('resources/front/fonts', 'public/fonts');
mix.copyDirectory('resources/front/images', 'public/global');
/**
 * plugins specific issue workaround for webpack
 * @see https://github.com/morrisjs/morris.js/issues/697
 * @see https://stackoverflow.com/questions/33998262/jquery-ui-and-webpack-how-to-manage-it-into-module
 */

mix.styles([
    'public/vendor/core/stimulsoft/css/stimulsoft.viewer.office2013.whiteteal.css',
    'public/vendor/core/stimulsoft/css/stimulsoft.designer.office2013.lightgrayteal.css'
], 'public/css/print_out.css');

mix.scripts([
    'public/vendor/core/stimulsoft/scripts/stimulsoft.reports.js',
    'public/vendor/core/stimulsoft/scripts/stimulsoft.reports.maps.js',
    'public/vendor/core/stimulsoft/scripts/stimulsoft.dashboards.js',
    'public/vendor/core/stimulsoft/scripts/stimulsoft.viewer.js',
    'public/vendor/core/stimulsoft/scripts/stimulsoft.designer.js'
], 'public/js/print_out.js');

mix.styles([
    'resources/front/css/plugins.css',
    'resources/front/css/style.css',
    'resources/front/css/responsive.css',
    'resources/front/js/plugins/revolution/css/settings.css',
    'resources/front/js/plugins/revolution/css/layers.css',
    'resources/front/js/plugins/revolution/css/navigation.css',
], 'public/css/front.css');

mix.scripts([
    'resources/front/js/jquery.js',
    'resources/front/js/plugins.min.js',
    'resources/front/js/functions.js',
    'resources/front/js/plugins/revolution/js/jquery.themepunch.tools.min.js',
    'resources/front/js/plugins/revolution/js/jquery.themepunch.revolution.min.js',
    'resources/front/js/plugins/revolution/js/extensions/revolution.extension.actions.min.js`',
    'resources/front/js/plugins/revolution/js/extensions/revolution.extension.kenburn.min.js',
    'resources/front/js/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js',
    'resources/front/js/plugins/revolution/js/extensions/revolution.extension.migration.min.js',
    'resources/front/js/plugins/revolution/js/extensions/revolution.extension.navigation.min.js',
    'resources/front/js/plugins/revolution/js/extensions/revolution.extension.parallax.min.js',
    'resources/front/js/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js',
    'resources/front/js/plugins/revolution/js/extensions/revolution.extension.video.min.js',
], 'public/js/front.js');

mix.webpackConfig({
    resolve: {
        alias: {
            'morris.js': 'morris.js/morris.js',
            'jquery-ui': 'jquery-ui',
        },
    },
});
